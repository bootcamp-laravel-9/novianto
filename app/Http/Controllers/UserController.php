<?php

namespace App\Http\Controllers;

use App\Models\Member;
use Illuminate\Http\Request;

class UserController extends Controller
{
    // /**
    //  * Display a listing of the resource.
    //  *
    //  * @return \Illuminate\Http\Response
    //  */
    public function index()
    {
        // $data = [
        //     [
        //         'nama' => 'Novianto',
        //         'universitas' => 'UAJY',
        //         'daerah' => 'Lubuklinggau'
        //     ],
        //     [
        //         'nama' => 'Christo',
        //         'universitas' => 'UAJY',
        //         'daerah' => 'Tegal'
        //     ],
        //     [
        //         'nama' => 'Bagas',
        //         'universitas' => 'UAJY',
        //         'daerah' => 'Kalimantan Tengah'
        //     ],
        //     [
        //         'nama' => 'Emanuel',
        //         'universitas' => 'UKDW',
        //         'daerah' => 'Tegal'
        //     ],
        //     [
        //         'nama' => 'Kiki',
        //         'universitas' => 'UKDW',
        //         'daerah' => 'Yogyakarta'
        //     ],
        //     [
        //         'nama' => 'Edwin',
        //         'universitas' => 'UKDW',
        //         'daerah' => 'Bantul'
        //     ],
        //     [
        //         'nama' => 'Samuel',
        //         'universitas' => 'UKDW',
        //         'daerah' => 'Purworejo'
        //     ],
        //     [
        //         'nama' => 'Ryzal',
        //         'universitas' => 'UPN',
        //         'daerah' => 'Sumedang'
        //     ],
        //     [
        //         'nama' => 'Fatur',
        //         'universitas' => 'UPN',
        //         'daerah' => 'Yogyakarta'
        //     ],
        //     [
        //         'nama' => 'Evan',
        //         'universitas' => 'UAJY',
        //         'daerah' => 'Solo'
        //     ],
        // ];


        $data = Member::all();


        return view('member/index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\View
     */
    public function show($id)
    {
        $data = Member::find($id);
        $isEdit = false;
        return view('member/show', compact('data', 'isEdit'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $data = Member::find($id);
        $isEdit = true;
        return view('member/show', compact('data', 'isEdit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Member::find($id);
        $data->name = $request->name;
        $data->univ = $request->univ;
        $data->asal = $request->asal;

        if ($data->save()) {
            return redirect()->route('member.index');
        } else {
            return redirect()->back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Member::destroy($id);
        return redirect()->route('member.index');
    }
}
