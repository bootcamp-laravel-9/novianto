@extends('layout/main')
@section('menu-biodata', 'active')
@section('content')
    <article class="resume-wrapper text-center position-relative">
        <div class="resume-wrapper-inner mx-auto text-start bg-white shadow-lg">
            <header class="resume-header pt-4 pt-md-0">
                <div class="row">
                    <div class="col-block col-md-auto resume-picture-holder text-center text-md-start">
                        <img class="picture h-75 bg-cover" src="{{ asset('/images/nathan.png') }}" alt="">
                    </div><!--//col-->
                    <div class="col">
                        <div class="row p-4 justify-content-center justify-content-md-between">
                            <div class="primary-info col-auto">
                                <h1 class="name mt-0 mb-1 text-white text-uppercase text-uppercase">Novianto</h1>
                                <div class="title mb-3">Frontend Developer</div>
                                <ul class="list-unstyled">
                                    <li class="mb-2"><a class="text-link"
                                            href="
                                            mailto:novianto778@gmail.com
                                        "><i
                                                class="far fa-envelope fa-fw me-2" data-fa-transform="grow-3"></i>
                                            novianto778@gmail.com
                                        </a></li>
                                    <li><a class="text-link" href="#"><i class="fas fa-mobile-alt fa-fw me-2"
                                                data-fa-transform="grow-6"></i> 0895605115938
                                        </a></li>
                                </ul>
                            </div><!--//primary-info-->
                            <div class="secondary-info col-auto mt-2">
                                <ul class="resume-social list-unstyled">
                                    <li class="mb-3"><a class="text-link" href="#"><span
                                                class="fa-container text-center me-2"><i
                                                    class="fab fa-linkedin-in fa-fw"></i></span>linkedin.com/in/noviantodev</a>
                                    </li>
                                    <li class="mb-3"><a class="text-link" href="#"><span
                                                class="fa-container text-center me-2"><i
                                                    class="fab fa-github-alt fa-fw"></i></span>github.com/novianto778</a>
                                    </li>

                                    <li><a class="text-link" href="#"><span class="fa-container text-center me-2"><i
                                                    class="fas fa-globe"></i></span>noviantodev.vercel.app</a></li>
                                </ul>
                            </div><!--//secondary-info-->
                        </div><!--//row-->

                    </div><!--//col-->
                </div><!--//row-->
            </header>
            <div class="resume-body p-5">
                <section class="resume-section summary-section mb-5">
                    <h2 class="resume-section-title text-uppercase font-weight-bold pb-3 mb-3">Career Summary</h2>
                    <div class="resume-section-content">
                        <p class="mb-0">I am a Front End Developer based in Yogyakarta, Indonesia. I have experience
                            in building web applications using NextJS, TailwindCSS and TypeScript.</p>
                    </div>
                </section><!--//summary-section-->
                <div class="row">
                    <div class="col-lg-9">
                        <section class="resume-section experience-section mb-5">
                            <h2 class="resume-section-title text-uppercase font-weight-bold pb-3 mb-3">Work Experience</h2>
                            <div class="resume-section-content">
                                <div class="resume-timeline position-relative">
                                    <article class="resume-timeline-item position-relative pb-5">

                                        <div class="resume-timeline-item-header mb-2">
                                            <div class="d-flex flex-column flex-md-row">
                                                <h3 class="resume-position-title font-weight-bold mb-1">Frontend Developer
                                                    <small class="text-muted">(Intern)</small>
                                                </h3>
                                                <div class="resume-company-name ms-auto">Berijalan</div>
                                            </div><!--//row-->
                                            <div class="resume-position-time">2024 - Present</div>
                                        </div><!--//resume-timeline-item-header-->
                                        <div class="resume-timeline-item-desc">
                                            <p>Bootcamp Technocenter Berijalan</p>

                                            <h4 class="resume-timeline-item-desc-heading font-weight-bold">Technologies
                                                used:</h4>
                                            <ul class="list-inline">
                                                <li class="list-inline-item"><span
                                                        class="badge bg-secondary badge-pill">React</span></li>
                                                <li class="list-inline-item"><span
                                                        class="badge bg-secondary badge-pill">React Native</span></li>
                                                <li class="list-inline-item"><span
                                                        class="badge bg-secondary badge-pill">Laravel</span></li>
                                                <li class="list-inline-item"><span
                                                        class="badge bg-secondary badge-pill">ASP.NET</span></li>
                                                <li class="list-inline-item"><span
                                                        class="badge bg-secondary badge-pill">Tailwind CSS</span></li>
                                                <li class="list-inline-item"><span
                                                        class="badge bg-secondary badge-pill">PostgresSQL</span></li>
                                            </ul>
                                        </div><!--//resume-timeline-item-desc-->

                                    </article><!--//resume-timeline-item-->

                                    <article class="resume-timeline-item position-relative pb-5">

                                        <div class="resume-timeline-item-header mb-2">
                                            <div class="d-flex flex-column flex-md-row">
                                                <h3 class="resume-position-title font-weight-bold mb-1">Frontend Web
                                                    Developer
                                                    <small class="text-muted">(Intern)</small>
                                                </h3>
                                                <div class="resume-company-name ms-auto">Telkom</div>
                                            </div><!--//row-->
                                            <div class="resume-position-time">Aug 2023 - Des 2023</div>
                                        </div><!--//resume-timeline-item-header-->
                                        <div class="resume-timeline-item-desc">
                                            <p>Internship at Digital Amoeba PT. Telkom Indonesia
                                                Worked passionately in Front-End Web Developer position and Technology
                                                Division
                                                Develop the "IdeaBox Multitenant," a website-based application for managing
                                                idea collectors within Telkom.
                                                Collaborate using the Agile Scrum methodology</p>

                                            <h4 class="resume-timeline-item-desc-heading font-weight-bold">Technologies
                                                used:</h4>
                                            <ul class="list-inline">
                                                <li class="list-inline-item"><span
                                                        class="badge bg-secondary badge-pill">Next.js</span></li>
                                                <li class="list-inline-item"><span
                                                        class="badge bg-secondary badge-pill">Redux</span></li>
                                                <li class="list-inline-item"><span
                                                        class="badge bg-secondary badge-pill">SASS</span></li>
                                                <li class="list-inline-item"><span
                                                        class="badge bg-secondary badge-pill">Formik</span></li>
                                                <li class="list-inline-item"><span
                                                        class="badge bg-secondary badge-pill">SCRUM</span></li>
                                            </ul>
                                        </div><!--//resume-timeline-item-desc-->

                                    </article><!--//resume-timeline-item-->

                                    <article class="resume-timeline-item position-relative pb-5">

                                        <div class="resume-timeline-item-header mb-2">
                                            <div class="d-flex flex-column flex-md-row">
                                                <h3 class="resume-position-title font-weight-bold mb-1">Frontend Developer
                                                    <small class="text-muted">(Freelance)</small>
                                                </h3>
                                                <div class="resume-company-name ms-auto">FTI UAJY</div>
                                            </div><!--//row-->
                                            <div class="resume-position-time">Sep 2022 - Jan 2023</div>
                                        </div><!--//resume-timeline-item-header-->
                                        <div class="resume-timeline-item-desc">
                                            <p>Frontend Developer at Project Industrial Professional Learning.

                                            </p>
                                            <p>Industrial Professional Learning is a website-based application that aims to
                                                validate and convert values ​​from professional learning activities carried
                                                out by students.
                                                - Developed a multiple-role dashboard using React.js, TailwindCSS and React
                                                Table.
                                                - Collaborating with a backend developer using tools like Trello for project
                                                management and Postman for API testing.</p>
                                            <h4 class="resume-timeline-item-desc-heading font-weight-bold">Technologies
                                                used:</h4>
                                            <ul class="list-inline">
                                                <li class="list-inline-item"><span
                                                        class="badge bg-secondary badge-pill">React.js</span></li>
                                                <li class="list-inline-item"><span
                                                        class="badge bg-secondary badge-pill">Tailwind CSS</span></li>
                                                <li class="list-inline-item"><span
                                                        class="badge bg-secondary badge-pill">React Query</span></li>

                                                <li class="list-inline-item"><span class="badge bg-secondary badge-pill">
                                                        Zustand
                                                    </span></li>
                                            </ul>
                                        </div><!--//resume-timeline-item-desc-->

                                    </article><!--//resume-timeline-item-->

                                    <article class="resume-timeline-item position-relative">

                                        <div class="resume-timeline-item-header mb-2">
                                            <div class="d-flex flex-column flex-md-row">
                                                <h3 class="resume-position-title font-weight-bold mb-1">Frontend Developer
                                                    <small class="text-muted">(Freelance)</small>
                                                </h3>
                                                <div class="resume-company-name ms-auto">Freelance</div>
                                            </div><!--//row-->
                                            <div class="resume-position-time">Sep 2022 - Jan 2023</div>
                                        </div><!--//resume-timeline-item-header-->
                                        <div class="resume-timeline-item-desc">
                                            <p>Pamdesku is a web-based application that aims to help the people of Gunung
                                                Kidul to record and pay water bills and allow partners to manage bills and
                                                payments.
                                                - Developed a multiple-role dashboard using React.js, TailwindCSS and React
                                                Table.
                                                - Collaborating with a backend developer using tools like Trello for project
                                                management and Postman for API testing.
                                                - Supervising and collaborating with a frontend developer to build different
                                                features using GitHub.</p>
                                            <h4 class="resume-timeline-item-desc-heading font-weight-bold">Technologies
                                                used:</h4>
                                            <ul class="list-inline">
                                                <li class="list-inline-item"><span
                                                        class="badge bg-secondary badge-pill">React.js</span></li>
                                                <li class="list-inline-item"><span
                                                        class="badge bg-secondary badge-pill">Tailwind CSS</span></li>
                                                <li class="list-inline-item"><span
                                                        class="badge bg-secondary badge-pill">React Query</span></li>

                                                <li class="list-inline-item"><span class="badge bg-secondary badge-pill">
                                                        Zustand
                                                    </span></li>
                                            </ul>
                                        </div><!--//resume-timeline-item-desc-->

                                    </article><!--//resume-timeline-item-->


                                </div><!--//resume-timeline-->






                            </div>
                        </section><!--//experience-section-->
                    </div>
                    <div class="col-lg-3">
                        <section class="resume-section skills-section mb-5">
                            <h2 class="resume-section-title text-uppercase font-weight-bold pb-3 mb-3">Skills &amp; Tools
                            </h2>
                            <div class="resume-section-content">
                                <div class="resume-skill-item">
                                    <ul class="list-unstyled mb-4">
                                        <li class="mb-2">
                                            Next.js
                                        </li>
                                        <li class="mb-2">
                                            React.js
                                        </li>
                                        <li class="mb-2">
                                            Tailwind CSS
                                        </li>
                                        <li class="mb-2">
                                            Typescript
                                        </li>
                                        <li class="mb-2">
                                            Node.js
                                        </li>
                                        <li class="mb-2">
                                            SQL
                                        </li>
                                    </ul>
                                </div><!--//resume-skill-item-->


                            </div><!--resume-section-content-->
                        </section><!--//skills-section-->
                        <section class="resume-section education-section mb-5">
                            <h2 class="resume-section-title text-uppercase font-weight-bold pb-3 mb-3">Education</h2>
                            <div class="resume-section-content">
                                <ul class="list-unstyled">
                                    <li class="mb-2">
                                        <div class="resume-degree font-weight-bold">
                                            Bachelor of Information System
                                        </div>
                                        <div class="resume-degree-org">Universitas Atma Jaya Yogyakarta</div>
                                        <div class="resume-degree-time">2020 - Present</div>
                                    </li>

                                </ul>
                            </div>
                        </section><!--//education-section-->
                        <section class="resume-section reference-section mb-5">
                            <h2 class="resume-section-title text-uppercase font-weight-bold pb-3 mb-3">Awards</h2>
                            <div class="resume-section-content">
                                <ul class="list-unstyled resume-awards-list">
                                    <li class="mb-2 ps-4 position-relative">
                                        <i class="resume-award-icon fas fa-trophy position-absolute"
                                            data-fa-transform="shrink-2"></i>
                                        <div class="resume-award-name">1st Place - Web Development Competition</div>
                                        <div class="resume-award-desc">
                                            Awarded the 1st place in the Web Development Competition held by the IFEST
                                        </div>
                                    </li>
                                    <li class="mb-0 ps-4 position-relative">
                                        <i class="resume-award-icon fas fa-trophy position-absolute"
                                            data-fa-transform="shrink-2"></i>
                                        <div class="resume-award-name">
                                            Kotakode Coding Festival Winner
                                        </div>
                                        <div class="resume-award-desc">
                                            Challenge to answer coding cases with the topic of Data Structures and
                                            Algorithms in Javascript.
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </section><!--//interests-section-->

                    </div>
                </div><!--//row-->
            </div><!--//resume-body-->


        </div>
    </article>
@endsection
