@extends('layout/main')
@section('heading', 'Edit Member')
@section('menu-member', 'active')
@section('content')
    <form>
        <div class="card-body">
            <div class="form-group">
                <label for="name">
                    Nama
                </label>
                <input type="text" class="form-control" id="name" placeholder="Enter email" value="{{ $data['name'] }}"
                    readonly>
            </div>
            <div class="form-group">
                <label for="univ">Asal Universitas</label>
                <input type="text" class="form-control" id="univ" placeholder="univ" readonly
                    value="{{ $data['univ'] ?? '-' }}">
            </div>

            <div class="form-group">
                <label for="univ">Asal Daerah</label>
                <input type="text" class="form-control" id="univ" placeholder="asal" readonly
                    value="{{ $data['asal'] ?? '-' }}">
            </div>

        </div>
        <!-- /.card-body -->
    </form>
@endsection
