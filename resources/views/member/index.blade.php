@extends('layout/main')
@section('menu-member', 'active')
@php
    $color = [
        'UAJY' => 'success',
        'UKDW' => 'primary',
        'UPN' => 'danger',
        '' => '',
    ];
@endphp
@section('content')
    <h4>
        Daftar Anggota Bootcamp Batch 9
    </h4>
    <a href="{{ route('member.create') }}" class="btn btn-primary">Tambah Anggota</a>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th style="width: 10px">#</th>
                <th>Nama</th>
                <th>Asal Universitas</th>
                <th>Asal Daerah</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            {{-- @for ($i = 0; $i < count($data); $i++)
                <tr>
                    <td>{{ $i + 1 }}.</td>
                    <td>{{ $data[$i]['nama'] }}</td>
                    <td>{{ $data[$i]['universitas'] }}</td>
                    <td>{{ $data[$i]['daerah'] }}</td>
                </tr>
            @endfor --}}

            @foreach ($data as $value)
                <tr>
                    <td>{{ $loop->iteration }}.</td>
                    <td>{{ $value['name'] }}</td>
                    <td>
                        <span class="badge badge-{{ $color[$value['univ']] }}">{{ $value['univ'] }}</span>
                    </td>
                    <td>{{ $value['asal'] }}</td>
                    <td>
                        <a href="{{ route('member.show', $value['id']) }}" class="btn btn-sm btn-info">Detail</a>
                        <a href="{{ route('member.edit', $value['id']) }}" class="btn btn-sm btn-warning">Edit</a>
                        <form action="{{ route('member.destroy', $value['id']) }}" method="POST" style="display: inline">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                        </form>
                </tr>
            @endforeach

        </tbody>
    </table>
@endsection
