@extends('layout/main')
@section('heading', $isEdit ? 'Edit Member' : 'Detail Member')
@section('menu-member', 'active')
@section('content')
    <form method="POST" action="{{ $isEdit ? route('member.update', $data['id']) : '#' }}">
        @csrf
        @if ($isEdit)
            @method('PUT')
        @endif
        <input type="hidden" name="id" value="{{ $data['id'] }}">
        <div class="card-body">
            <div class="form-group">
                <label for="name">
                    Nama
                </label>
                <input type="text" class="form-control" name="name" id="name" placeholder="Enter email"
                    value="{{ $data['name'] }}" {{ $isEdit ? '' : 'disabled' }}>

            </div>
            <div class="form-group">
                <label for="univ">Asal Universitas</label>
                <input type="text" class="form-control" name="univ" id="univ" placeholder="univ"
                    value="{{ $data['univ'] ?? '-' }}" {{ $isEdit ? '' : 'disabled' }}>
            </div>

            <div class="form-group">
                <label for="univ">Asal Daerah</label>
                <input type="text" class="form-control" name="asal" id="asal" placeholder="asal"
                    value="{{ $data['asal'] ?? '-' }}" {{ $isEdit ? '' : 'disabled' }}>
            </div>

        </div>
        @if ($isEdit)
            <!-- /.card-body -->
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a type='button' class="btn btn-danger" href="{{ route('member.index') }}">Cancel</a>
            </div>
        @else
            <div class="card-footer">
                <a type='button' class="btn btn-primary" href="{{ route('member.edit', $data['id']) }}">Edit</a>
                <a type='button' class="btn btn-danger" href="{{ route('member.index') }}">Back</a>
            </div>
        @endif
        <!-- /.card-body -->
    </form>
@endsection
