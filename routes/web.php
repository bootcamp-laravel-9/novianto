<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/', function () {
    return view('dashboard/index');
})->name('dashboard');


// Route::get('/table', 'UserController@index')->name('table');
Route::get('/biodata', 'BiodataController@Index')->name('biodata');

Route::resource('member', 'UserController')->names('member');
// ->name('index', 'table')
// ->name('create', 'table.create')
// ->name('store', 'table.store')
// ->name('show', 'table.show')
// ->name('edit', 'table.edit')
// ->name('update', 'table.update')
// ->name('destroy', 'table.destroy');

// auth
Route::get('/login', 'AuthController@login')->name('login');
Route::get('/register', 'AuthController@register')->name('register');
