<?php

namespace Database\Seeders;

use App\Models\Member;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MemberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Member::factory(3)->create();

        $members = [
            [
                'name' => 'Novianto',
            ],
            [
                'name' => 'Christo',
            ],
            [
                'name' => 'Bagas'
            ]
        ];

        DB::beginTransaction();

        foreach ($members as $member) {
            Member::firstOrCreate($member);
        }

        DB::commit();
    }
}
